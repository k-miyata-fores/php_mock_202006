<?php

ini_set('disaplay_errors',1);
require 'pdo.php';

if (isset($_GET['search'])) {
    $search = htmlspecialchars($_GET['search']);
    $search_value = $search;
  }else {
    $search = '';
    $search_value = '';
  }

$sql = 'select * from documents where id = :id';
$stmt = $pdo->prepare($sql);//プリペアドステートメント
$stmt->bindvalue('id',$search,PDO::PARAM_INT);//紐付け
$stmt->execute();//実行

$result = $stmt->fetchAll();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">

    <title>検索</title>
</head>
<body>
<form action="" method="get">
    <p>書類の検索</p>
    <input type="text" name="search" value="<?php echo $search_value ?>"><br>
    <input type="submit" name="" value="検索">
</form>
    

<p><strong>書類について（仮のバージョンでbootstrap,fontawesomeをcdnで使用しています。）</strong></p>
<p>書類アイコンは：<i class="fa fa-file" aria-hidden="true"></i></p>
<p>フォルダアイコンは：<i class="fa fa-folder" aria-hidden="true"></i></i></p>

<table class="table">
<thead>
    <th>id</th>
    <th>書類コード</th>
    <th>ユーザー番号</th>
    <th>氏名</th>
    <th>者間契約</th>
    <th>書類の状態</th>
</thead>

<?php foreach ($result as $key) { ?>
    <td><?php echo $key['id']; ?></td>
    <td><?php echo $key['doccode']; ?></td>
    <td><?php echo $key['name']; ?></td>
    <td><?php echo $key['customer_acco_id']; ?></td>
    <td><?php echo $key['target_count']; ?></td>
    <td><?php echo $key['status_cd']; ?></td>
<?php } ?>

</table>
    </body>
</html>