<?php

    const DB_HOST = 'mysql:dbname=great_sign_demo;port=3306;host=localhost;charset=utf8';
    const DB_USER = 'great_sign_admin'; 
    const DB_PASS = 'root'; 

    try {

        $pdo = new PDO(DB_HOST,DB_USER,DB_PASS, [
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, //連想配列で取得
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, //例外
            PDO::ATTR_EMULATE_PREPARES => false //SQLインジェクション
        ]);

        echo 'DB接続成功';

    } catch (PDOException $e) {
        echo 'DB接続エラー' . $e->getMessage();
        exit();
    }


?>